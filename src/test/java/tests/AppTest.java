package tests;

import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.appium.java_client.android.AndroidDriver;
import data.*;

public class AppTest extends AbstractTestNGCucumberTests
{
	/////////////BDD DATA////////////////////////////////////////////////
 /*   String name = data.LoadProperties.BDD.getProperty("name");
	String email = data.LoadProperties.BDD.getProperty("email"); 
	String password = data.LoadProperties.BDD.getProperty("password"); 
	String phone = data.LoadProperties.BDD.getProperty("phone"); 
	String address = data.LoadProperties.BDD.getProperty("address");*/
	
	
	/////////////////////////ELEMENTS///////////////////////////
	public WebElement appTxt;
	public WebElement appbtn;
	public WebElement skipebtn;
	public WebElement signupbtn;
	public WebElement nametxt;
	public WebElement emailtxt;
	public WebElement passtxt;
	public WebElement phonetxt;
	public WebElement agetxt;
	public WebElement okbtn;
	public WebElement addresstxt;
	public WebElement femalerad;
	public WebElement signbtn;
	public WebElement successbtn;
	public WebElement skipbtn2;
	public WebElement emtxt;
	public WebElement patxt;
	public WebElement signinbtn;
	public WebElement sucesstxt;


	public void capability(String x, String y , String z , String f , String t ) throws Throwable
	{
		//////////capabilities////////////////////////
		
	DesiredCapabilities de = new DesiredCapabilities();
	de.setCapability("platformName", "Android");
	de.setCapability("platformVersion", "7.1.1");
	de.setCapability("deviceName", "Pixel");
	AndroidDriver<WebElement> dr = new AndroidDriver<>(new URL("http://0.0.0.0:4723/wd/hub"),de);
	/////////////////////we can write path of app here ////////////////////////////////////
	
	
	////////////////////////// find-elements//////////////////////////////////////////////////////
	
	appTxt = dr.findElement(By.id("com.google.android.apps.nexuslauncher:id/all_apps_handle"));
    appbtn = dr.findElement(By.xpath("(//android.widget.TextView[@content-desc=\"BeFit 360\"])[2]"));
	skipebtn = dr.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup"));
	signupbtn = dr.findElement(By.id("com.robusta.befit:id/sign_up_btn"));
	nametxt= dr.findElement(By.id("com.robusta.befit:id/textinput_placeholder"));
	emailtxt= dr.findElement(By.id("com.robusta.befit:id/email_et"));
	passtxt= dr.findElement(By.id("ccom.robusta.befit:id/password_et"));
	phonetxt= dr.findElement(By.id("com.robusta.befit:id/phone_number_et"));
	agetxt= dr.findElement(By.id("com.robusta.befit:id/age_etr"));
	okbtn= dr.findElement(By.id("android:id/button1"));
	addresstxt= dr.findElement(By.id("com.robusta.befit:id/address_et"));
	femalerad= dr.findElement(By.id("com.robusta.befit:id/female_radio_btn"));
	signbtn= dr.findElement(By.id("com.robusta.befit:id/sign_up_btn"));
    successbtn= dr.findElement(By.id("com.robusta.befit:id/btn_popup_done"));
    skipbtn2= dr.findElement(By.id("com.robusta.befit:id/tv_skip"));
    emtxt= dr.findElement(By.id("com.robusta.befit:id/edt_email")); 
    patxt= dr.findElement(By.id("com.robusta.befit:id/edt_password"));
    signinbtn= dr.findElement(By.id("com.robusta.befit:id/btn_sign_in"));
    sucesstxt= dr.findElement(By.id("com.robusta.befit:id/toolbar_title"));
	}
	
	@Test(priority=1,alwaysRun=true)	
	public void appium_test(String name,String email,String password,String phone,String address) throws Throwable 
	{
		try 
		{
		capability(name,email,password,phone,address);
		appTxt.click();
		appbtn.click();
		skipebtn.click();
		signupbtn.click();
		nametxt.sendKeys(name);
		emailtxt.sendKeys(email);
		passtxt.sendKeys(password);
		phonetxt.sendKeys(phone);
		addresstxt.sendKeys(address);
		agetxt.click();
		okbtn.click();
		femalerad.click();
		signbtn.click();
		Assert.assertEquals(successbtn.getText(),"OK");
		successbtn.click();
		skipbtn2.click();
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
		
	}
	
	
	@Test(dependsOnMethods= {"appium_test"})
	public void login_test(String email,String password) throws Throwable 
	{
		try 
		{
		emtxt.sendKeys(email);
		patxt.sendKeys(password);
		signinbtn.click();
		Assert.assertEquals(sucesstxt.getText(),"PROFILE");
		} 
		catch (Exception e) 
		{
			System.out.println(e.getCause());
			
		}
		
        
	}

	

}