package steps;


import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tests.AppTest;


public class UserRegistration extends AppTest {

	
	AppTest App = new AppTest();
	 

	@Given("^the user in the welcome screen$")
	public void the_user_in_the_welcome_screen() throws Throwable 
	{
		appTxt.click();
		appbtn.click();
		skipebtn.click();
		Assert.assertTrue(skipebtn.isDisplayed());
	}

	@When("^I click on register button$")
	public void i_click_on_register_buton() 
	{
		Assert.assertTrue(signupbtn.isDisplayed());
		signupbtn.click();
	}


	@When("^I entered \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\" , \"([^\"]*)\", \"([^\"]*)\"$")
	public void i_entered(String name, String email, String password, String phone , String address) throws Throwable 
	{
		App.capability(name,email,password,phone,address);
		nametxt.sendKeys(name);
		emailtxt.sendKeys(email);
		passtxt.sendKeys(password);
		phonetxt.sendKeys(phone);
		addresstxt.sendKeys(address);
		agetxt.click();
		okbtn.click();
		femalerad.click();
		signbtn.click();
		
	}

	@Then("^The registration screen displayed successfully$")
	public void the_registration_screen_displayed_successfully()  
	{
		Assert.assertEquals(successbtn.getText(),"OK");
		successbtn.click();
		skipbtn2.click();
		
	}
}
