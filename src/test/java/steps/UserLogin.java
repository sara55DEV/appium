package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import tests.AppTest;


public class UserLogin extends AppTest 
{
	
	AppTest App = new AppTest();

	@Given("^user is on sign screen$")
	public void user_is_on_sign_sceen() 
	{
		Assert.assertTrue(emtxt.isEnabled());
	}

	@When("^I entered \"([^\"]*)\" , \"([^\"]*)\"$")
	public void I_entered(String email ,String password ) throws Throwable 
	{
		emtxt.sendKeys(email);
		patxt.sendKeys(password);
		signinbtn.click();
		Assert.assertEquals(sucesstxt.getText(),"PROFILE");
	}

	

	@Then("^The Home screen displayed successfully$")
	public void The_Home_screen_displayed_sucessfully() throws InterruptedException
	{
		Assert.assertTrue(sucesstxt.isDisplayed());
	}
}
