Feature: User Registration
   I want to check that the user can register in our fitness app.
   
	Scenario Outline: User Registration
	Given the user in the welcome screen
	When I click on register button
	And I entered "<name>" , "<email>" ,"<password>","<phone>","<address>" 
	Then The registration screen displayed successfully
 
 	Examples:
 	 | name        | email                       | password  | phone        |address  |
 	 | sarapostman | bejesi7095@uniteditcare.com | Queen@123 | +201118876747|nasr city|
 	 
 	  